package com.pichs.xuikit.raised

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import androidx.annotation.ColorInt
import androidx.constraintlayout.widget.ConstraintLayout
import com.pichs.common.widget.utils.XDisplayHelper
import com.pichs.xuikit.R
import kotlin.math.*

/**
 * 6666
 */
class RaisedLayout @JvmOverloads constructor(context: Context, var attrs: AttributeSet? = null, var defStyleAttr: Int = 0) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var mPaint = Paint()
    private var mArcPath = Path()

    // 大圆的圆新
    private var bigCircleCenterPoint = PointF()

    private var startPoint = PointF()
    private var qiePointLeft = PointF()
    private var qiePointRight = PointF()
    private var endPoint = PointF()

    private var qieRectFLeft = RectF() // 左边切圆的外切正方形坐标
    private var qieRectFRight = RectF() // 右边切圆的外切正方形坐标
    private var circleRectF = RectF() // 大圆的外切正方形坐标
    private var longSquareRectF = RectF() // 底部长方体坐标

    private var W = 0f // 总宽
    private var H = 0f // 总高
    private var h1 = 0f // 底部非凸起高

    // 中间圆角的大小
    private var r = 0f // 中间园半径
    private var r1 = 0f // 倒圆角，相切园半径

    private var r0 = 0f // 两个相切圆心之间距离
    private var r0v = 0f // 切圆圆心到大圆圆心垂直距离
    private var r0h = 0f // 切圆圆心到大圆圆心水平距离
    private var arcRads = 0f

    private var startY = 0f // y轴开始坐标

    @ColorInt
    private var bgColor = Color.TRANSPARENT

    private var shadowColor = Color.TRANSPARENT
    private var shadowElevation = 0
    private var shadowOffset = 0
    private var circleRadius = 0
    private var cornerCircleRadius = 0
    private var contentHeight = 0
    private var circleMarginTop = 0
    private var circleMarginBottom = 0

    // 圆形左边距离view左边距
    private var circleMarginLeft = 0f

    // 大圆圆心在X轴的比例，0  -  1f, 默认 0.5f 中间
    private var widthMaxProgress = 0

    private var circleCurrentProgress = 1

    init {
        if (attrs != null) {
            initAttrs()
        }
        initData()
    }

    @SuppressLint("Recycle")
    private fun initAttrs() {
        // 处理属性数据
        val ta = context.resources.obtainAttributes(attrs, R.styleable.RaisedLayout)
        if (ta != null) {
            bgColor = ta.getColor(R.styleable.RaisedLayout_xp_backgroundColor, Color.TRANSPARENT)
            shadowColor = ta.getColor(R.styleable.RaisedLayout_xp_shadowColor, Color.TRANSPARENT)
            shadowElevation = ta.getDimensionPixelSize(R.styleable.RaisedLayout_xp_shadowElevation, 0)
            shadowOffset = ta.getDimensionPixelSize(R.styleable.RaisedLayout_xp_shadowOffset, 0)
            circleRadius = ta.getDimensionPixelSize(R.styleable.RaisedLayout_xp_raised_circle_radius, 0)
            circleMarginTop = ta.getDimensionPixelSize(R.styleable.RaisedLayout_xp_raised_circle_marginTop, 0)
            circleMarginBottom = ta.getDimensionPixelSize(R.styleable.RaisedLayout_xp_raised_circle_marginBottom, 0)
            widthMaxProgress = ta.getInteger(R.styleable.RaisedLayout_xp_raised_width_maxProgress, 0)
            circleCurrentProgress = ta.getInteger(R.styleable.RaisedLayout_xp_raised_circle_currentProgress, circleCurrentProgress)
            circleMarginLeft = ta.getDimensionPixelSize(R.styleable.RaisedLayout_xp_raised_circle_marginLeft, 0).toFloat()
            circleRadius = ta.getDimensionPixelSize(R.styleable.RaisedLayout_xp_raised_circle_radius, 0)
            cornerCircleRadius =
                    ta.getDimensionPixelSize(R.styleable.RaisedLayout_xp_raised_corner_circle_radius, 0)
            contentHeight = ta.getDimensionPixelSize(R.styleable.RaisedLayout_xp_raised_content_height, 0)
            ta.recycle()
            if (circleCurrentProgress < 1) {
                circleCurrentProgress = 1
            }
            if (widthMaxProgress > 0) {
                if (circleCurrentProgress > widthMaxProgress) {
                    circleCurrentProgress = widthMaxProgress
                }
            }
        }
    }

    private fun initData() {
        // 赋值部分
        mPaint.isAntiAlias = true
        mPaint.color = bgColor
        mPaint.style = Paint.Style.FILL
        mPaint.strokeWidth = 1f
        W = XDisplayHelper.getScreenWidth(context).toFloat()
        mPaint.setShadowLayer(shadowElevation.toFloat(), 0f, -shadowOffset.toFloat(), shadowColor)
        // 计算坐标等数据
//        calculateSize()
    }


    private fun calculateSize() {

        r = circleRadius.toFloat()
        r1 = cornerCircleRadius.toFloat()
        h1 = contentHeight.toFloat()

        if (circleCurrentProgress < 1) {
            circleCurrentProgress = 1
        }
        if (widthMaxProgress > 0) {
            if (circleCurrentProgress > widthMaxProgress) {
                circleCurrentProgress = widthMaxProgress
            }
            circleMarginLeft = (circleCurrentProgress.toFloat() - 0.5f) * (W / widthMaxProgress.toFloat()) - r
        } else {
            if (circleMarginLeft <= 0) {
                circleMarginLeft = W / 2f - r
            }
        }
        // 计算部分
        r0 = r + r1
        startY = H - h1
        r0v = r1 + h1 - r - circleMarginBottom.toFloat()
        r0h = sqrt(r0 * r0 - r0v * r0v)
        // 圆形中心坐标
        bigCircleCenterPoint.set(circleMarginLeft + r, H - r - circleMarginBottom)

        // 给初始化节点
        startPoint.set(bigCircleCenterPoint.x - r0h, startY)

        // 计算 arc画线弧度 例:60度，一圈360度
        arcRads = calArcRadiusByValue((r0h / r0).toDouble())
        val r1dx1 = r1 * calArcSinValue(arcRads.toDouble()) // 根据夹角计算对边长
        val r1dy1 = r1 * calArcCosValue(arcRads.toDouble()) // 根据夹角计算临边长
        // 设置切点坐标
        qiePointLeft.set(startPoint.x + r1dx1, startPoint.y - (r1 - r1dy1))
        qiePointRight.set(bigCircleCenterPoint.x + r0h - r1dx1, qiePointLeft.y)
        endPoint.set(bigCircleCenterPoint.x + r0h, startY)

        // 计算切圆的四个坐标
        qieRectFLeft.set(startPoint.x - r1, startY - 2 * r1, startPoint.x + r1, startY)
        qieRectFRight.set(endPoint.x - r1, startY - 2 * r1, endPoint.x + r1, startY)


        circleRectF.set(bigCircleCenterPoint.x - r, bigCircleCenterPoint.y - r, bigCircleCenterPoint.x + r, bigCircleCenterPoint.y + r)
        // 长方形的范围
        longSquareRectF.set(0f, startPoint.y, W, H)

//        XLogKt.d {
//            """
//                000000000=====>
//
//                宽：${W}
//                高：${H}
//                shadowHeight：${shadowHeight}
//
//                长方形高 h1：${h1}
//
//                大圆半径 r ： ${r}
//                切圆半径 r1 ： ${r1}
//                圆心距离 r0 ： ${r0}
//
//                r0v ： ${r0v}
//                r0h ： ${r0h}
//
//                画圆弧角度 arcRads r0 ： $arcRads
//
//                大圆的圆心坐标X: bigPoint.x: ${bigPoint.x}
//                大圆的圆心坐标Y: bigPoint.y: ${bigPoint.y}
//
//                startPoint.x ： ${startPoint.x}
//                startPoint.y ： ${startPoint.y}
//
//                左侧切点X: qiePointLeft.x ： ${qiePointLeft.x}
//                左侧切点Y: qiePointLeft.y ： ${qiePointLeft.y}
//
//                右侧切点X: qiePointRight.x ： ${qiePointRight.x}
//                右侧切点Y: qiePointRight.y ： ${qiePointRight.y}
//
//            """
//        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        W = MeasureSpec.getSize(widthMeasureSpec).toFloat()
        val dh = (2 * r) + shadowElevation + circleMarginBottom + circleMarginTop
        H = if (dh < contentHeight) contentHeight.toFloat() else dh
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(H.toInt(), MeasureSpec.EXACTLY))
        calculateSize()
    }

//    override fun onDraw(canvas: Canvas?) {
//        drawBackground(canvas)
//    }

    override fun dispatchDraw(canvas: Canvas?) {
        drawBackground(canvas)
        super.dispatchDraw(canvas)
    }

    private fun drawBackground(canvas: Canvas?) {
        canvas?.apply {
            save()
            if (h1 <= r * 2) {
                mArcPath.moveTo(longSquareRectF.left, longSquareRectF.top)
                mArcPath.lineTo(startPoint.x, startPoint.y)
                mArcPath.arcTo(qieRectFLeft, 90f, -arcRads)
                mArcPath.arcTo(circleRectF, 270f - arcRads, arcRads * 2)
                mArcPath.arcTo(qieRectFRight, 90f + arcRads, -arcRads)

                mArcPath.lineTo(longSquareRectF.right, longSquareRectF.top)
                mArcPath.lineTo(longSquareRectF.right, longSquareRectF.bottom)
                mArcPath.lineTo(longSquareRectF.left, longSquareRectF.bottom)
                mArcPath.lineTo(longSquareRectF.left, longSquareRectF.top)
                drawPath(mArcPath, mPaint)
            } else {
                drawRect(longSquareRectF, mPaint)
            }
            restore()
        }
    }

    companion object {
        // 角度转正弦系数 Math.sin(arc*radRatio)
        private var radRatio = Math.PI / 180

        // 正弦转角度系数 Math.asin(d) * radRatioReverse
        private var radRatioReverse = 180 / Math.PI

        private fun calArcSinValue(arcRadius: Double): Float {
            return sin(arcRadius * radRatio).toFloat()
        }

        private fun calArcCosValue(arcRadius: Double): Float {
            return cos(arcRadius * radRatio).toFloat()
        }

        private fun calArcRadiusByValue(value: Double): Float {
            return (asin(value) * radRatioReverse).toFloat()
        }

        // 填充
        const val STYLE_FILL = 0

        // 轮廓
        const val STYLE_STROKE = 1

        // 填充加轮廓
        const val STYLE_FILL_AND_STROKE = 2
    }


    fun setBgColor(@ColorInt color: Int) {
        bgColor = color
        mPaint.color = bgColor
        postInvalidate()
    }

    fun setShadowColor(@ColorInt color: Int) {
        shadowColor = color
        mPaint.setShadowLayer(shadowElevation.toFloat(), 0f, shadowOffset.toFloat(), shadowColor)
        postInvalidate()
    }


    @JvmOverloads
    fun setBgStyle(style: Int, strokeWidth: Float = 0f) {
        mPaint.strokeWidth = strokeWidth
        when (style) {
            STYLE_FILL -> {
                mPaint.style = Paint.Style.FILL
            }
            STYLE_STROKE -> {
                mPaint.style = Paint.Style.STROKE
            }
            STYLE_FILL_AND_STROKE -> {
                mPaint.style = Paint.Style.FILL_AND_STROKE
            }
        }
        postInvalidate()
    }


    fun setCircleMarginTop(margin: Int) {
        circleMarginTop = margin
        postInvalidate()
    }


    fun setCircleMarginBottom(margin: Int) {
        circleMarginBottom = margin
        postInvalidate()
    }

    fun setCircleMarginLeft(margin: Int) {
        circleMarginLeft = margin.toFloat()
        postInvalidate()
    }

    fun setShadowElevation(elevation: Int) {
        shadowElevation = elevation
        mPaint.setShadowLayer(shadowElevation.toFloat(), 0f, shadowOffset.toFloat(), shadowColor)
        postInvalidate()
    }

    fun setShadowOffset(offset: Int) {
        shadowOffset = offset
        mPaint.setShadowLayer(shadowElevation.toFloat(), 0f, shadowOffset.toFloat(), shadowColor)
        postInvalidate()
    }


    fun setCircleRadius(radius: Int) {
        circleRadius = radius
        postInvalidate()
    }

    fun setCircleCornerRadius(radius: Int) {
        cornerCircleRadius = radius
        postInvalidate()
    }

    fun setContentHeight(height: Int) {
        contentHeight = height
        postInvalidate()
    }

    fun setWidthMaxProgress(progress: Int) {
        widthMaxProgress = progress
        postInvalidate()
    }

    fun setCurrentProgress(progress: Int) {
        circleCurrentProgress = progress
        postInvalidate()
    }


}